<?php

declare(strict_types=1);

namespace App\Application\Task\Event;

use App\Domain\Task\Task;

class TaskAssigned
{
    private Task $task;

    public function __construct(Task $task)
    {
        $this->task = $task;
    }

    public function getTask(): Task
    {
        return $this->task;
    }
}
