<?php

declare(strict_types=1);

namespace App\Application\Task\Event\Handler;

use App\Application\Notification\Notifier;
use App\Application\Task\Event\TaskAssigned;
use App\Domain\Notification\Notification;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class TaskAssignedHandler implements MessageHandlerInterface
{
    private Notifier $notifier;

    public function __construct(Notifier $notifier)
    {
        $this->notifier = $notifier;
    }

    public function __invoke(TaskAssigned $taskAssigned)
    {
        $task = $taskAssigned->getTask();
        $user = $task->getAssignee();
        if (!$user) {
            return;
        }

        $this->notifier->send(
            new Notification('Task has been assigned', 'Task: "'.$task->getName().'" assigned', $user)
        );
    }
}
